class CreateAds < ActiveRecord::Migration[6.0]
  def change
    create_table :ads do |t|
      t.string :Title
      t.string :Description
      t.string :Content
      t.integer :creator_id

      t.timestamps
    end
  end
end
