class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.string :First_name
      t.string :Last_name
      t.string :Gender
      t.string :Address
      t.date :DOB
      t.string :Organization
      t.integer :Contact
      t.string :Website
      t.text :Description

      t.timestamps
    end
  end
end
