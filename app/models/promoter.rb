class Promoter < User
    has_many :requests
    has_many :promotions
    has_many :ads, :through => :promotions
end
