class Ad < ApplicationRecord
    has_many :requests
    belongs_to :creator
    has_many :promotions
    has_many :promoters, :through => :promotion
end