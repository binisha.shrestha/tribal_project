class User < ApplicationRecord
    has_one :profile
    has_many :requests
    has_many :ads
    validates :username, length:{minimum: 3, maximum: 30}, presence: true
    validates :email, email: true 
    validates :password, presence: true, length: {minimum: 8, maximum: 40} 
    validates :type, presence: true

    #follow
    has_many :active_follows, class_name: "Follow", foreign_key: "follower_id", dependent: :destroy
    has_many :passive_follows, class_name: "Follow", foreign_key: "followed_id", dependent: :destroy
    
    has_many :following, through: :active_follows, source: :followed
    has_many :followers, through: :passive_follows, source: :follower

    def follow(user)
        active_follows.create(followed_id: user.id)
    end

    def unfollow(user)
        active_follows.find_by(followed_id: user.id).destroy
    end

    # def following?(user)
    #     following.include(user)
    # end
end
